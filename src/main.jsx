import React from "react";
import ReactDOM from "react-dom/client";
import { DecodeApp } from "./DecodeApp";
import "./index.css";
import { HashRouter as Router } from "react-router-dom";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <Router>
      <DecodeApp />
    </Router>
  </React.StrictMode>
);
