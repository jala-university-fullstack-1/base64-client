import { Route, Routes } from "react-router";
import ErrorPage from "./components/ErrorPage";
import Form from "./components/Form";
import { decodeBase64Data, encodeBase64Data } from "./utilities/ApiUtil";

export const DecodeApp = () => {
  return (
    <div className="card">
      <h1 className="title">Encode ➖ Decode</h1>
      <p className="description">
        Application to encode and decode your messages, perfect to leave a coded
        message to someone 😶‍🌫️
      </p>
      <Routes>
        <Route>
          <Route
            path="/"
            element={<Form name="Encode" apiMethod={encodeBase64Data} />}
          />
          <Route
            path="/encode"
            element={<Form name="Encode" apiMethod={encodeBase64Data} />}
          />
          <Route
            path="/decode"
            element={<Form name="Decode" apiMethod={decodeBase64Data} />}
          />
          <Route path="*" element={<ErrorPage />} />
        </Route>
      </Routes>
    </div>
  );
};
