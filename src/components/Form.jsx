import { useState } from "react";
import Swal from "sweetalert2";
import SwitchButton from "./SwitchButton";
import "bootstrap/dist/css/bootstrap.min.css";

const Form = ({ name, apiMethod }) => {
  const [input, setInput] = useState("");
  const [result, setResult] = useState("");

  const submitHandler = async (event) => {
    event.preventDefault();

    if (input) {
      try {
        const response = await apiMethod(input);
        setResult(response.input);
      } catch (err) {
        Swal.fire({
          icon: "error",
          title: "Error " + err.response.status,
          text: err.response.data.title,
        });
      }
    }
  };

  const reset = () => {
    setInput("");
    setResult("");
  };

  return (
    <>
      <form onSubmit={submitHandler}>
        <div className="container">
          <br />
          <SwitchButton/>
          <br />
          <br />
          <h3>
            {name} {name === "Encode" ? "✍️" : "🤖"}
          </h3>
          <textarea
            value={input}
            onChange={(e) => setInput(e.target.value)}
            className="border"
            cols="50"
            rows="5"
          />
          <br />
          <h3>Output:</h3>
          <div className="result">{result}</div>
        </div>
        <br />
        <br />
        <input
          className="button green-color"
          type="submit"
          value="Execute ✅"
          disabled={!input}
        />
        <button onClick={reset} className="button red-color">
          Clear 🗑️
        </button>
      </form>
    </>
  );
};

export default Form;
