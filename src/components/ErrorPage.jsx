import { Link } from "react-router-dom";

export default function ErrorPage() {
  return (
    <div id="error-page">
      <i className="fa-solid fa-face-sad-cry emoji"></i>
      <h1>Oops!</h1>
      <p>Sorry, an unexpected error has occurred.</p>
      <p>
        <i className="orientation-buttons">
          <Link className="tag encode-color" to="/encode">
            Go to encode ✍️
          </Link>
          <br />
          <br />
          <Link className="tag decode-color" to="/decode">
            Go to decode 🤖
          </Link>
        </i>
      </p>
    </div>
  );
}
