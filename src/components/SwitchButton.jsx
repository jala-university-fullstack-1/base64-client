import "bootstrap/dist/css/bootstrap.min.css";
import ToggleButton from "react-bootstrap/ToggleButton";
import ToggleButtonGroup from "react-bootstrap/ToggleButtonGroup";
import { Link } from "react-router-dom";
import { useLocation } from "react-router-dom";

export default function SwitchButton() {
  const location = useLocation();
  console.log(location.pathname === "/encode");
  return (
    <>
      <ToggleButtonGroup type="radio" name="options" defaultValue={1}>
        <Link to="/encode">
          <ToggleButton
            className="button decode-color"
            id="tbg-radio-1"
            value={1}
          >
            Encode ✍️
          </ToggleButton>
        </Link>
        <Link to="/decode">
          <ToggleButton
            className="button encode-color"
            id="tbg-radio-2"
            value={2}
          >
            Decode 🤖
          </ToggleButton>
        </Link>
      </ToggleButtonGroup>
    </>
  );
}
