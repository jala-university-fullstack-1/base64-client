import axios from "axios";
axios.defaults.baseURL = import.meta.env.VITE_REACT_APP_API_URL;

export const encodeBase64Data = async (Input) => {
  const response = await axios.post("/api/v1/base64/encode", { Input });
  console.log(response.status);
  return response.data;
};

export const decodeBase64Data = async (Input) => {
  const response = await axios.post("/api/v1/base64/decode", { Input });
  console.log(response.status);
  return response.data;
};
